class slate {

	package { 'Slate':
		ensure   => installed,
		provider => macapp,
		source   => 'https://github.com/mattr-/slate/releases/download/v1.2.0/Slate.zip',
	}

	define install_slate_js {

		if ($name != 'root') and ($name != 'Shared') {

			file { "install slate.js for ${name}":
				ensure => present,
				path   => "/Users/${name}/.slate.js",
				source => "puppet:///modules/slate/slate.js",
			}

		}
	}

	if $users != '' {
		$users_split = split($users, "\n")
		install_slate_js { $users_split: }
	}

}
